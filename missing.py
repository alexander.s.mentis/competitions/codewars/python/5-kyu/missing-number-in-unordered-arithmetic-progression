import itertools

def pairwise(iterable):
    # pairwise('ABCDEFG') --> AB BC CD DE EF FG
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def find(seq):
    seq = sorted(seq)
    lo_0, hi_0 = seq[0], seq[1]
    init_span = hi_0 - lo_0
    
    for lo, hi in pairwise(seq):
        span = hi-lo
        if span > init_span:
            return lo + span//2
        elif init_span > span:
            return lo_0 + init_span//2
        # else spans are equal, continue
        
    return None # should not reach